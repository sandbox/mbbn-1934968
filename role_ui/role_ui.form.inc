<?php

function  role_ui_form(&$form, &$form_state){
  if(isset($form['term_data']['#term'])){      
    // Build the sortable table header.
      
    $grant_view=array(        
        '#type' => 'fieldset',        
        '#title' => t('Grant View'),                    
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#weight' => 10,
        '#prefix' => '<div>',
        '#suffix' => '</br></div>',
        );
    $tmp=$form['term_data']['field_grant_view'];
    $tmp['#prefix']="</br>";
    unset($tmp['und']['#title']);
    unset($form['term_data']['field_grant_view']);
    $form['term_data']['grant_view']=$grant_view;
    $form['term_data']['grant_view']['field_grant_view']=$tmp;
      
    role_member_table($form,$form['term_data']['#term']['name']);        
    permission_table($form,$form['term_data']['#term']['name']);          
    }    
}

function role_member_table(&$form,$role_name){
    /* 
     * Table Of Members     
     */     
    $form['term_data']['members'] = array(        
        '#type' => 'fieldset',        
        '#title' => t('Members Of Role'),                    
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#weight' => 10,
        '#prefix' => '<div>',
        '#suffix' => '</br></div>',
        );      
    
    $form['term_data']['members']['table'] = array(        
        '#prefix' => '</br><table>',
        '#suffix' => '</table>',
        );  
    $form['term_data']['members']['table']['header'] =array(
        array(
            '#type' => 'item',
            '#title' => t('#'),
            '#prefix' => '<tr><th>',
            '#suffix' => '</th>',
            ),
        array(
            '#type' => 'item',
            '#title' => t('User Name'),
            '#prefix' => '<th>',
            '#suffix' => '</th>',
            ),
        array(
            '#type' => 'item',
            '#title' => t('Expire'),
            '#prefix' => '<th>',
            '#suffix' => '</th>',
        ),        
    );
    
    $members= role_manager_members($role_name);
    $i=0;    
    foreach ($members as $member){
        $form['term_data']['members']['table'][$i]=array(
            '#prefix' => '<tr>',
            '#suffix' => '</tr>',
        );
        $form['term_data']['members']['table'][$i]['checkbox']=array(
            '#type' => 'checkbox',
            '#prefix' => '<td>',
            '#suffix' => '</td>',
        );
        $form['term_data']['members']['table'][$i]['user name']=array(
            '#type' => 'link',
            '#title' => $member['name'],
            '#href' => 'user/'.$member['uid'],
            '#prefix' => '<td>',
            '#suffix' => '</td>',
        );
        $form['term_data']['members']['table'][$i]['expire_'.$member['uid']]=array(
            '#type' => 'date_select',
            '#default_value' => ($member['expire']!='-1')?date('Y-m-d H',$member['expire']):NULL,
            '#date_format' => 'Y-m-d H',
            '#date_label_position' => 'within',
            '#prefix' => '<td>',
            '#suffix' => '</td>',
        );
        $i++;
    }
    
    $form['term_data']['members']['userName'] = array(
        '#type' => 'textfield',                                                        
        '#size' => 30,                     
        '#autocomplete_path' => 'user/autocomplete',
        '#prefix' => '</table><table><tr><td>',                    
        '#suffix' => '</td>',
        );
                
    $form['term_data']['members']['add_button'] = array(
        '#attributes' => array('class' => array('taxonomy-manager-buttons','add')),
        '#type' => 'button',
        '#value' => t('Add'),
        '#theme' => 'no_submit_button',
        '#ajax' => array(
            'callback' => 'add_member',
            'event' => 'click',
            'wrapper' => 'edit-members',
            'method' => 'replace',
            'effect' => 'fade',
            ),                    
        '#prefix' => '<td>',                    
        '#suffix' => '</td>',
        );    
    
    $form['term_data']['members']['del_button'] = array(
        '#attributes' => array('class' => array('taxonomy-manager-buttons','delete')),
        '#type' => 'button',
        '#value' => t('Remove Selected'),
        '#theme' => 'no_submit_button',
        '#ajax' => array(
            'callback' => 'delete_member',
            'event' => 'click',
            'wrapper' => 'checkboxes-div', 
            'method' => 'replace',
            'effect' => 'fade',
            ),                    
        '#prefix' => '<td>',                    
        '#suffix' => '</td>',
        );
    $form['term_data']['members']['apply_button'] = array(
        //'#attributes' => array('class' => array('taxonomy-manager-buttons','delete')),
        '#type' => 'button',
        '#value' => t('Apply'),
        '#theme' => 'no_submit_button',
        '#ajax' => array(
            'callback' => 'apply_change',
            'event' => 'click',
            'wrapper' => 'checkboxes-div', 
            'method' => 'replace',
            'effect' => 'fade',
            ),                    
        '#prefix' => '<td>',                    
        '#suffix' => '</td></tr></table>',
        );
}

function permission_table(&$form,$role_name){
    /*
     * Table of Permission
     */
    $form['term_data']['permission'] = array(
        '#type' => 'fieldset',
        '#title' => t('Permission Of Role'),                    
        '#collapsible' => TRUE,
        '#collapsed' => TRUE,
        '#weight' => 11,
        '#prefix' => '<div>',
        '#suffix' => '</br></div>',
        );
    
    $form['term_data']['permission']['table']=array(
        '#type' => 'item',
        '#prefix' => '<table>',
        '#suffix' => '</table>',
        );     

    $form['term_data']['permission']['table']['header']=array(
        array(
            '#type' => 'item',
            '#title' => t('Permission'),
            '#prefix' => '<tr><th>',
            '#suffix' => '</th>',
            ),
        array(
            '#type' => 'item',
            '#title' => t('Enable'),
            '#prefix' => '<th>',
            '#suffix' => '</th>',
            ),
        array(
            '#type' => 'item',
            '#title' => t('Grant'),
            '#prefix' => '<th>',
            '#suffix' => '</th></tr>',
        ),
    );
    $permissions=  role_manager_login_permissions();
    if(count($permissions)){
        $assined=  role_manager_permissions($role_name);
        $i=0;
        foreach ($permissions as $permission){
            $i++;
            $form['term_data']['permission']['table']['row'.$i]=array(
                '#prefix' => '<tr>',
                '#suffix' => '</tr>',
            );
            $form['term_data']['permission']['table']['row'.$i]['name']=array(
                '#type' => 'item',
                '#title' => t($permission->permission),
                '#prefix' => '<td>',
                '#suffix' => '</td>',
            );            
            $key=str_replace(" ","_",$permission->permission);
            $form['term_data']['permission']['table']['row'.$i][$key]=array(
                '#type' => 'checkboxes',
            );
            
            $form['term_data']['permission']['table']['row'.$i][$key]['assigned']=array(
                '#type' => 'checkbox',
                '#default_value' => isset($assined[$permission->permission]),
                '#prefix' => '<td>',
                '#suffix' => '</td>',
            );
            $form['term_data']['permission']['table']['row'.$i][$key]['grant']=array(
                '#type' => 'checkbox',
                '#default_value' => isset($assined[$permission->permission])?$assined[$permission->permission]:FALSE,
                //'#disabled' => 'true',
                '#states' => array(
                    'enabled' => array(
                        ':input[name="assigned"]' => array('checked' => TRUE),
                    ),
                ),
                '#prefix' => '<td>',
                '#suffix' => '</td>',
            );
        }
    } else {
        $form['term_data']['permission']['table']['empty_row']=array(
            array(
                '#type' => 'item',
                '#title' => t('Empty'),
                '#prefix' => '<tr><td>',
                '#suffix' => '</td>',
                ),
            array(
                '#type' => 'item',                
                '#prefix' => '<td>',
                '#suffix' => '</td>',
                ),
            array(
                '#type' => 'item',                
                '#prefix' => '<td>',
                '#suffix' => '</td></tr>',
                ),
            );        
       }
        
        $form['term_data']['permission']['submit']=array(
            '#attributes' => array('class' => array('taxonomy-manager-buttons','save')),
            '#type' => 'button',
            '#value' => t('Save Permissions'),
            '#theme' => 'no_submit_button',
            '#ajax' => array(
                'callback' => 'save_permissions',
                'event' => 'click',
                //'wrapper' => 'checkboxes-div',            
                'method' => 'replace',
                'effect' => 'fade',
                ),                    
            '#prefix' => '<td>',                    
            '#suffix' => '</td></tr></table>',
        );
}

function add_member($form,&$form_state){   
    $form_state['cache']=false;
    if(isset($form_state['values']['userName'])){
        $userName=$form_state['values']['userName'];        
        $user=user_load_by_name($userName);        
        $role=  user_role_load_by_name($form_state['term']->name);
        if(!isset($user->roles[$role->rid])){
            $roles = $user->roles + array($role->rid => $role->name);
            user_save($user, array('roles' => $roles));
        }
        if(count($user->field_roles)){
            $user->field_roles['und'][]=array('tid'=> $form_state['term']->id);
        } else {
            $user->field_roles['und'] = array(
                array('tid'=> $form_state['term']->id)
                );
        }
     }               
}

function delete_member($form,$form_state){
    $members=$form['term_data']['members']['role_member']['#options'];
    $role_name=$form_state['term']->name;
    $users=$form_state['values']['role_member'];
    foreach ($users as $user){
        $user=user_load_by_name($user);
        $role=  user_role_load_by_name($role_name);
        user_multiple_role_edit($user, 'remove_role', $role->rid);        
        }        
}

function apply_change($form,$form_state){
    $role_name=$form_state['term']->name;
    $role=  user_role_load_by_name($role_name);
    $expire=array();
    global $user;
    $timezone=$user->timezone;
    foreach ($form_state['input'] as $key => $input){
        if(preg_match('/^expire/', $key)){
            $key=  explode('_', $key);
            $isTime= $input['year'] | $input['month']|$input['day']|$input['hour'];
            $time=$input['year'].'-'.$input['month'].'-'.$input['day'].' '.$input['hour'].':00:00';
            $timeStamp=($isTime)?date_timestamp_get(new DateTime($time,new DateTimeZone($timezone))):0;
            $expire[$key[1]]=$timeStamp;
        }
    }
    foreach ($expire as $key => $val){
        if(!$val){
            continue;
        }
        $result=  db_update('users_roles')
                ->fields(array(
                    'expire' => $val
                    ))
                ->condition('uid',$key,'=')
                ->condition('rid',$role->rid,'=')
                ->execute();
    }
}

function save_permissions($form,$form_state){
    $login_permissions= role_manager_login_permissions();  
    foreach ($login_permissions as $permision){ 
        $key=  str_replace(" ","_", $permision->permission);
        if(isset($form_state['input'][$key])){
            if(isset($form_state['input'][$key]['assigned'])){
                $assign[$permision->permission]=$form_state['input'][$key]['assigned'];
                } else {
                    $assign[$permision->permission]=0;
                    }            
        }
    }    
    $grant=array();
    foreach ($assign as $key=>$val){
        $input_key=  str_replace(" ","_", $key);
        if($val != 0 && !isset($grant[$key])){
            $grant[$key]=isset($form_state['input'][$input_key]['grant'])?$form_state['input'][$input_key]['grant']:0;            
            }
        }
    
     $role=  user_role_load_by_name($form_state['term']->name);    
    if(!empty($assign)) {
        user_role_change_permissions($role->rid,$assign);         
        role_manager_change_permissions_grant($role->rid, $grant);
    }    
}

function role_manager_change_permissions_grant($rid, $grant=NULL){
    if($grant==NULL){
        return db_update("role_permission")
        ->fields(
                array(
                    'isgrant' => 1,
                    )
                )
            ->condition('rid',$rid,'=')
            ->execute();
    }
    
    $granted=  array_filter($grant);
    if($granted){
        $result=db_update("role_permission")        
        ->fields(
                array(
                    'isgrant' => 1,
                    )
                )
        ->condition('permission',  array_keys($grant),'IN')
        ->condition('rid',$rid,'=')
        ->execute();        
        }
    
    $revoke = array_diff_assoc($grant, $granted);
    if($revoke){
        $result=db_update("role_permission")
        ->fields(
                array(
                    'isgrant' => 1,
                    )
                )
        ->condition('permission', array_keys($revoke),'IN')
        ->condition('rid',$rid,'=')
        ->execute();   
    }
}
