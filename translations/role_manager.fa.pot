# $Id$
#
# Persian translation of Drupal (general)
# Copyright YEAR NAME <EMAIL@ADDRESS>
# Generated from files:
#  role_manager.install: n/a
#  role_manager.info: n/a
#
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2013-02-20 13:27+0330\n"
"PO-Revision-Date: 2013-02-20 13:46+0330\n"
"Last-Translator: mbbn <biabani.mohammad@gmail.com>\n"
"Language-Team: Persian <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n!=1);\n"
"X-Generator: Poedit 1.5.4\n"

#: role_manager.install:10
msgid "Role Tree"
msgstr "درخت سمت ها"

#: role_manager.info:0
msgid "Role Manager"
msgstr "مدیر سمت ها"

#: role_manager.info:0
msgid "Role Management"
msgstr "مدیریت سمت ها"
